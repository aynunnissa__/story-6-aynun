from django.contrib import admin
from django.contrib.auth.models import User
from .models import Kegiatan, Peserta

admin.site.register([Kegiatan,Peserta])