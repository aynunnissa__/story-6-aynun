from django.urls import path

from . import views

app_name = 'story6'
urlpatterns = [
    path('', views.kegiatan, name='kegiatan'),
    path('tambahKegiatan/', views.tambahKegiatan, name='tambahKegiatan'),
    path('tambahPeserta/<int:id>', views.tambahPeserta, name='tambahPeserta'),
    path('deleteKegiatan/<int:id>', views.deleteKegiatan, name="deleteKegiatan"),
]