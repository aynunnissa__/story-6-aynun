from django.test import TestCase, Client
from .models import Kegiatan, Peserta
from .views import tambahKegiatan, tambahPeserta
from .forms import KegiatanForm, PesertaForm
from django.test import LiveServerTestCase, TestCase, tag
from selenium import webdriver

class KegiatanTestCase(TestCase):
	def test_url_kegiatan(response):
		kegiatan = Client().get('/')
		tambahKegiatan = Client().get('/tambahKegiatan/')

		response.assertEquals(200, kegiatan.status_code)
		response.assertEquals(200, tambahKegiatan.status_code)

	def test_template_kegiatan(response):		
		kegiatan = Client().get('/')
		tambahKegiatan = Client().get('/tambahKegiatan/')
		response.assertTemplateUsed(kegiatan, 'story6/listKegiatan.html')
		response.assertTemplateUsed(tambahKegiatan, 'story6/tambahKegiatan.html')

	def test_form_valid(self):
		title = "Bernyanyi"
		form_data = {'title' : title}
		form = KegiatanForm(data=form_data)
		self.assertTrue(form.is_valid())

	def test_form_peserta_valid(self):
		nama_peserta = "Joko"
		form_data = {'nama_peserta' : nama_peserta}
		form = PesertaForm(data=form_data)
		self.assertTrue(form.is_valid())		

	def test_func_tambah_kegiatan(self):
		title = "Bernyanyi"
		form_data = {'title' : title}
		request = Client().get('/tambahKegiatan/')
		request.POST = form_data
		request.method = "POST"
		tambah = tambahKegiatan(request)
		tambah.client = Client()
		self.assertRedirects(tambah, '/')

	def test_func_tambah_peserta(self):
		Kegiatan.objects.create(title="kegiatanQu")
		kegiatan = Kegiatan.objects.filter(title="kegiatanQu")[0]
		id = kegiatan.id
		nama_peserta = "Joko"
		form_data = {'nama_peserta' : nama_peserta}
		param = '/tambahPeserta/' + str(id)
		request = Client().get(param)
		request.POST = form_data
		request.method = "POST"
		tambah = tambahPeserta(request,id)
		tambah.client = Client()
		self.assertRedirects(tambah, '/')