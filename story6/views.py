from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from django.views.generic import TemplateView
from .forms import KegiatanForm, PesertaForm
from .models import Kegiatan, Peserta


def kegiatan(request):
	events = Kegiatan.objects.all()
	pesertaForm = PesertaForm()
	return render(request, 'story6/listKegiatan.html', {'kegiatan' : events,'pesertaForm' : pesertaForm})

def tambahKegiatan(request) :
	if request.method == 'POST':
		form = KegiatanForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('story6:kegiatan')
	else:
		form = KegiatanForm()
	return render(request, 'story6/tambahKegiatan.html', {'form' : form})

def tambahPeserta(request,id) :
	if request.method == 'POST':
		form = PesertaForm(request.POST)
		if form.is_valid():
			kegiatan = get_object_or_404(Kegiatan, id=id)
			kegiatan.peserta_set.create(nama_peserta=form.cleaned_data.get('nama_peserta'))
			return redirect('story6:kegiatan')
	else:
		pesertaForm = PesertaForm()
	return render(request, 'story6/listKegiatan.html', {'pesertaForm' : pesertaForm})

def deleteKegiatan(request, id):
	kegiatan = get_object_or_404(Kegiatan, id=id)
	kegiatan.delete()
	return redirect('story6:kegiatan')