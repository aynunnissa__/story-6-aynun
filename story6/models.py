from django.db import models
from django.contrib.auth.models import User

class Kegiatan(models.Model) :
	title = models.CharField(max_length=100)

	def __str__(self):
		return self.title

class Peserta(models.Model) :
	kegiatan = models.ForeignKey(Kegiatan, on_delete=models.CASCADE)
	nama_peserta = models.CharField(max_length=100)

	def __str__(self):
		return self.nama_peserta