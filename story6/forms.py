from django import forms
from django.contrib.auth.models import User
from .models import Kegiatan, Peserta
from django.db import models

class KegiatanForm(forms.ModelForm):

	class Meta:
		model = Kegiatan
		fields = ['title']

class PesertaForm(forms.ModelForm):

	class Meta:
		model = Peserta
		fields = ['nama_peserta']